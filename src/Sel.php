<?php

namespace Drupal\sel;

/**
 * Provides Sel defaults and helper methods.
 */
class Sel {

  /**
   * The available defaults for the rel attribute.
   *
   * @return array
   *   The available defaults labels, keyed by the attribute value.
   */
  public static function relDefaults() {
    return [
      'noreferrer' => t('No referrer (noreferrer)'),
      'noopener' => t('No opener (noopener)'),
    ];
  }

  /**
   * Returns the optional rel attributes.
   *
   * @return array
   *   An array of optional valid rel attribute value names  keyed by attribute
   *   value.
   */
  public static function relOptionals() {
    return [
      'external' => t('External (external)'),
      'nofollow' => t('Nofollow (nofollow)'),
    ];
  }

  /**
   * Flattens a multidimensional array.
   *
   * @param array $array
   *   The (potentially) multidimensional source array.
   *
   * @return array
   *   The flat array.
   */
  public static function flatArray(array $array) {
    $flattened = [];

    foreach ($array as $array_key => $array_value) {
      if (!is_array($array_value)) {
        $flattened[$array_key] = $array_value;
      }
      else {
        foreach ($array_value as $array_subkey => $array_subvalue) {
          if (!is_array($array_subvalue)) {
            $flattened[$array_subkey] = $array_subvalue;
          }
          else {
            $flattened[$array_subkey] = self::flatArray($array_subvalue);
          }
        }
      }
    }

    return $flattened;
  }

}
