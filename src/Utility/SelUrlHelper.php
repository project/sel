<?php

namespace Drupal\sel\Utility;

use Drupal\Core\Url;

/**
 * Helper class for external URLs.
 *
 * @ingroup utility
 */
class SelUrlHelper {

  /**
   * Determines whether an Url is external to Drupal.
   *
   * @param \Drupal\Core\Url $url
   *   The Url to check.
   *
   * @return bool
   *   TRUE or FALSE, where TRUE indicates an external path.
   */
  public static function isExternal(Url $url) {
    if (!$url->isExternal()) {
      return FALSE;
    }

    $uri_string = $url->toUriString();

    // We have to know our current host (pattern).
    static $host_pattern;
    if (!isset($host_pattern)) {
      $host = \Drupal::request()->getHost();
      $host_pattern = preg_quote($host);
    }

    // Now get the Uri string.
    $uri_string_w_trailing_slash = rtrim($uri_string, '/') . '/';

    return preg_match('/^[^\/]*:?\/\/' . $host_pattern . '\/.*$/u', $uri_string_w_trailing_slash) === 0;
  }

  /**
   * Determines whether a URI string is external to Drupal.
   *
   * @param string $uri_string
   *   The URI string to check.
   *
   * @return bool
   *   TRUE or FALSE, where TRUE indicates an external path.
   */
  public static function isExternalUriString($uri_string) {
    try {
      $url = Url::fromUri($uri_string);
    }
    catch (\Exception $e) {
      return FALSE;
    }

    return static::isExternal($url);
  }

}
