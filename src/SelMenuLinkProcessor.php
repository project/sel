<?php

namespace Drupal\sel;

use Drupal\sel\Utility\SelUrlHelper;
use Drupal\Core\Url;

/**
 * Utility class for processing external menu links.
 */
class SelMenuLinkProcessor {

  /**
   * Processes menu links.
   *
   * @param array $items
   *   Menu link items.
   */
  public static function processMenuLinks(array &$items) {
    $sel_settings = \Drupal::config('sel.settings');
    $rel = $sel_settings->get('menu_links.rel');
    $rel_required = !empty($rel) && in_array($rel, array_keys(Sel::relDefaults())) ?
      $rel : 'noreferrer';

    $rel_values = $sel_settings->get('menu_links.rel_optionals') ?: [];
    array_unshift($rel_values, $rel_required);

    foreach ($items as $id => $item) {
      /* @var \Drupal\Core\Url $item['url'] */
      if (
        $item['url'] instanceof Url &&
        SelUrlHelper::isExternal($item['url'])
      ) {
        $url_attributes = $item['url']->getOption('attributes') ?: [];
        $url_attributes['target'] = '_blank';

        foreach ($rel_values as $rel_value) {
          if (
            empty($url_attributes['rel']) ||
            strpos($url_attributes['rel'], $rel_value) === FALSE
          ) {
            $url_attributes['rel'] = empty($url_attributes['rel']) ?
              $rel_value :
              $url_attributes['rel'] . ' ' . $rel_value;
          }
        }

        $items[$id]['url']->setOption('attributes', $url_attributes);
      }

      if (!empty($item['below'])) {
        self::processMenuLinks($item['below']);
      }
    }
  }

}
