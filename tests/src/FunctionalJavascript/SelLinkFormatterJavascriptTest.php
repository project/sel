<?php

namespace Drupal\Tests\sel\FunctionalJavascript;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\link\LinkItemInterface;
use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;
use Drupal\Tests\sel\Traits\SelTestEntityDisplayTrait;
use Drupal\user\Entity\Role;
use Drupal\user\RoleInterface;

/**
 * Tests that email links are accessible with js.
 *
 * @group link
 */
class SelLinkFormatterJavascriptTest extends WebDriverTestBase {

  use SelTestEntityDisplayTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = [
    'user',
    'node',
    'link',
    'spamspan',
    'sel',
  ];

  /**
   * A field to use in this test class.
   *
   * @var \Drupal\field\Entity\FieldStorageConfig
   */
  protected $fieldStorage;

  /**
   * The instance used in this test class.
   *
   * @var \Drupal\field\Entity\FieldConfig
   */
  protected $field;

  /**
   * The test node.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected $node;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->drupalCreateContentType([
      'type' => 'test_page',
      'name' => 'Test page',
    ]);

    $field_name = mb_strtolower($this->randomMachineName());
    // Create a field with settings to validate.
    $this->fieldStorage = FieldStorageConfig::create([
      'field_name' => $field_name,
      'entity_type' => 'node',
      'type' => 'link',
      'cardinality' => FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED,
    ]);
    $this->fieldStorage->save();
    $this->field = FieldConfig::create([
      'field_storage' => $this->fieldStorage,
      'bundle' => 'test_page',
      'settings' => [
        'title' => DRUPAL_OPTIONAL,
        'link_type' => LinkItemInterface::LINK_GENERIC,
      ],
    ]);
    $this->field->save();
    $this->getEntityFormDisplay('node', 'test_page', 'default')
      ->setComponent($field_name, [
        'type' => 'link_default',
        'settings' => [],
      ])
      ->save();
    $this->getEntityViewDisplay('node', 'test_page', 'default')
      ->setComponent($field_name, [
        'type' => 'sel_link',
      ])
      ->save();

    // Create a new node.
    $node = Node::create([
      'type' => 'test_page',
      'title' => $this->randomString(),
    ]);
    $node->set($field_name, [
      [
        'uri' => 'mailto:an.user@example.com',
        'title' => '',
      ],
      [
        'uri' => 'mailto:an.user@example.com',
        'title' => 'Email link',
      ],
    ]);
    $node->save();
    assert($node instanceof NodeInterface);
    $this->node = $node;

    $anonymous = Role::load(RoleInterface::ANONYMOUS_ID);
    $anonymous->grantPermission('access content');
  }

  /**
   * Tests link filter.
   */
  public function testEmailLinkSanitizerFormatter(): void {
    $this->drupalGet($this->node->toUrl());

    // Check that email links are processed properly by the formatter.
    $links = $this->getSession()->getPage()->findAll('css', 'a.spamspan');
    // Verify simple 'mailto:an.user@example.com' link.
    $this->assertEquals('mailto:an.user@example.com', $links[0]->getAttribute('href'));
    $this->assertEquals('an.user@example.com', $links[0]->getText());
    // Verify 'mailto:an.user@example.com' link with title 'Email link'.
    $this->assertEquals('mailto:an.user@example.com', $links[1]->getAttribute('href'));
    $this->assertEquals('Email link', $links[1]->getText());
  }

}
