<?php

namespace Drupal\Tests\sel\Traits;

/**
 * Provides test links and expectations for Sel tests.
 */
trait SelLinkTestCasesTrait {

  /**
   * Test uris.
   *
   * @return array
   *   Common test cases and expectations for url-based tests.
   */
  protected function selTestUris(): array {
    return [
      //
      // Internal links.
      //
      0 => [
        'case' => [
          'uri' => 'route:<front>',
          'filter' => '<a href="/">Internal relative link (route:)</a>',
          'link' => [
            'link_text' => NULL,
            'menu_link_title' => 'Internal relative link (route:)',
          ],
        ],
        'expectations' => [
          'external' => FALSE,
          'filter' => '<a href="/">Internal relative link (route:)</a>',
          'link' => [
            'attributes' => [
              'target' => NULL,
              'rel' => NULL,
              'href' => '/',
            ],
            'link_text' => '/',
            'menu_link_title' => 'Internal relative link (route:)',
          ],
        ],
      ],
      1 => [
        'case' => [
          'uri' => 'internal:/<front>',
          'filter' => '<a href="/">Internal relative link (internal:)</a>',
          'link' => [
            'link_text' => NULL,
            'menu_link_title' => 'Internal relative link (internal:)',
          ],
        ],
        'expectations' => [
          'external' => FALSE,
          'filter' => '<a href="/">Internal relative link (internal:)</a>',
          'link' => [
            'attributes' => [
              'target' => NULL,
              'rel' => NULL,
              'href' => '/',
            ],
            'link_text' => '/',
            'menu_link_title' => 'Internal relative link (internal:)',
          ],
        ],
      ],
      //
      // Protocol-relative absolute internal links.
      //
      2 => [
        'case' => [
          'uri' => '//local.test',
          'filter' => '<a href="//local.test">Internal protocol-relative absolute link</a>',
          'link' => [
            'link_text' => NULL,
            'menu_link_title' => 'Internal protocol-relative absolute link',
          ],
        ],
        'expectations' => [
          'external' => FALSE,
          'filter' => '<a href="//local.test">Internal protocol-relative absolute link</a>',
          'link' => [
            'attributes' => [
              'target' => NULL,
              'rel' => NULL,
              'href' => '//local.test',
            ],
            'link_text' => '//local.test',
            'menu_link_title' => 'Internal protocol-relative absolute link',
          ],
        ],
      ],
      3 => [
        'case' => [
          'uri' => '//local.test/',
          'filter' => '<a href="//local.test/">Internal protocol-relative absolute link with trailer slash</a>',
          'link' => [
            'link_text' => NULL,
            'menu_link_title' => 'Internal protocol-relative absolute link with trailer slash',
          ],
        ],
        'expectations' => [
          'external' => FALSE,
          'filter' => '<a href="//local.test/">Internal protocol-relative absolute link with trailer slash</a>',
          'link' => [
            'attributes' => [
              'target' => NULL,
              'rel' => NULL,
              'href' => '//local.test/',
            ],
            'link_text' => '//local.test/',
            'menu_link_title' => 'Internal protocol-relative absolute link with trailer slash',
          ],
        ],
      ],
      4 => [
        'case' => [
          'uri' => '//local.test/user',
          'filter' => '<a href="//local.test/user">Internal protocol-relative absolute link to user page</a>',
          'link' => [
            'link_text' => NULL,
            'menu_link_title' => 'Internal protocol-relative absolute link to user page',
          ],
        ],
        'expectations' => [
          'external' => FALSE,
          'filter' => '<a href="//local.test/user">Internal protocol-relative absolute link to user page</a>',
          'link' => [
            'attributes' => [
              'target' => NULL,
              'rel' => NULL,
              'href' => '//local.test/user',
            ],
            'link_text' => '//local.test/user',
            'menu_link_title' => 'Internal protocol-relative absolute link to user page',
          ],
        ],
      ],
      5 => [
        'case' => [
          'uri' => '//local.test/user/login/',
          'filter' => '<a href="//local.test/user/login/">Internal protocol-relative absolute link to login page, with trailer slash</a>',
          'link' => [
            'link_text' => NULL,
            'menu_link_title' => 'Internal protocol-relative absolute link to login page, with trailer slash',
          ],
        ],
        'expectations' => [
          'external' => FALSE,
          'filter' => '<a href="//local.test/user/login/">Internal protocol-relative absolute link to login page, with trailer slash</a>',
          'link' => [
            'attributes' => [
              'target' => NULL,
              'rel' => NULL,
              'href' => '//local.test/user/login/',
            ],
            'link_text' => '//local.test/user/login/',
            'menu_link_title' => 'Internal protocol-relative absolute link to login page, with trailer slash',
          ],
        ],
      ],
      //
      // Absolute internal links with http.
      //
      6 => [
        'case' => [
          'uri' => 'http://local.test',
          'filter' => '<a href="http://local.test">Internal absolute http link</a>',
          'link' => [
            'link_text' => NULL,
            'menu_link_title' => 'Internal absolute http link',
          ],
        ],
        'expectations' => [
          'external' => FALSE,
          'filter' => '<a href="http://local.test">Internal absolute http link</a>',
          'link' => [
            'attributes' => [
              'target' => NULL,
              'rel' => NULL,
              'href' => 'http://local.test',
            ],
            'link_text' => 'http://local.test',
            'menu_link_title' => 'Internal absolute http link',
          ],
        ],
      ],
      7 => [
        'case' => [
          'uri' => 'http://local.test/',
          'filter' => '<a href="http://local.test/">Internal absolute http link with trailer slash</a>',
          'link' => [
            'link_text' => NULL,
            'menu_link_title' => 'Internal absolute http link with trailer slash',
          ],
        ],
        'expectations' => [
          'external' => FALSE,
          'filter' => '<a href="http://local.test/">Internal absolute http link with trailer slash</a>',
          'link' => [
            'attributes' => [
              'target' => NULL,
              'rel' => NULL,
              'href' => 'http://local.test/',
            ],
            'link_text' => 'http://local.test/',
            'menu_link_title' => 'Internal absolute http link with trailer slash',
          ],
        ],
      ],
      8 => [
        'case' => [
          'uri' => 'http://local.test/user',
          'filter' => '<a href="http://local.test/user">Internal absolute http link to user page</a>',
          'link' => [
            'link_text' => NULL,
            'menu_link_title' => 'Internal absolute http link to user page',
          ],
        ],
        'expectations' => [
          'external' => FALSE,
          'filter' => '<a href="http://local.test/user">Internal absolute http link to user page</a>',
          'link' => [
            'attributes' => [
              'target' => NULL,
              'rel' => NULL,
              'href' => 'http://local.test/user',
            ],
            'link_text' => 'http://local.test/user',
            'menu_link_title' => 'Internal absolute http link to user page',
          ],
        ],
      ],
      9 => [
        'case' => [
          'uri' => 'http://local.test/user/login/',
          'filter' => '<a href="http://local.test/user/login/">Internal absolute http link to login page, with trailer slash</a>',
          'link' => [
            'link_text' => NULL,
            'menu_link_title' => 'Internal absolute http link to login page, with trailer slash',
          ],
        ],
        'expectations' => [
          'external' => FALSE,
          'filter' => '<a href="http://local.test/user/login/">Internal absolute http link to login page, with trailer slash</a>',
          'link' => [
            'attributes' => [
              'target' => NULL,
              'rel' => NULL,
              'href' => 'http://local.test/user/login/',
            ],
            'link_text' => 'http://local.test/user/login/',
            'menu_link_title' => 'Internal absolute http link to login page, with trailer slash',
          ],
        ],
      ],
      //
      // Absolute internal links with https.
      //
      10 => [
        'case' => [
          'uri' => 'https://local.test',
          'filter' => '<a href="https://local.test">Internal absolute https link</a>',
          'link' => [
            'link_text' => NULL,
            'menu_link_title' => 'Internal absolute https link',
          ],
        ],
        'expectations' => [
          'external' => FALSE,
          'filter' => '<a href="https://local.test">Internal absolute https link</a>',
          'link' => [
            'attributes' => [
              'target' => NULL,
              'rel' => NULL,
              'href' => 'https://local.test',
            ],
            'link_text' => 'https://local.test',
            'menu_link_title' => 'Internal absolute https link',
          ],
        ],
      ],
      11 => [
        'case' => [
          'uri' => 'https://local.test/',
          'filter' => '<a href="https://local.test/">Internal absolute https link with trailer slash</a>',
          'link' => [
            'link_text' => NULL,
            'menu_link_title' => 'Internal absolute https link with trailer slash',
          ],
        ],
        'expectations' => [
          'external' => FALSE,
          'filter' => '<a href="https://local.test/">Internal absolute https link with trailer slash</a>',
          'link' => [
            'attributes' => [
              'target' => NULL,
              'rel' => NULL,
              'href' => 'https://local.test/',
            ],
            'link_text' => 'https://local.test/',
            'menu_link_title' => 'Internal absolute https link with trailer slash',
          ],
        ],
      ],
      12 => [
        'case' => [
          'uri' => 'https://local.test/user',
          'filter' => '<a href="https://local.test/user">Internal absolute https link to user page</a>',
          'link' => [
            'link_text' => NULL,
            'menu_link_title' => 'Internal absolute https link to user page',
          ],
        ],
        'expectations' => [
          'external' => FALSE,
          'filter' => '<a href="https://local.test/user">Internal absolute https link to user page</a>',
          'link' => [
            'attributes' => [
              'target' => NULL,
              'rel' => NULL,
              'href' => 'https://local.test/user',
            ],
            'link_text' => 'https://local.test/user',
            'menu_link_title' => 'Internal absolute https link to user page',
          ],
        ],
      ],
      13 => [
        'case' => [
          'uri' => 'https://local.test/user/login/',
          'filter' => '<a href="https://local.test/user/login/">Internal absolute https link to login page, with trailer slash</a>',
          'link' => [
            'link_text' => NULL,
            'menu_link_title' => 'Internal absolute https link to login page, with trailer slash',
          ],
        ],
        'expectations' => [
          'external' => FALSE,
          'filter' => '<a href="https://local.test/user/login/">Internal absolute https link to login page, with trailer slash</a>',
          'link' => [
            'attributes' => [
              'target' => NULL,
              'rel' => NULL,
              'href' => 'https://local.test/user/login/',
            ],
            'link_text' => 'https://local.test/user/login/',
            'menu_link_title' => 'Internal absolute https link to login page, with trailer slash',
          ],
        ],
      ],
      //
      // Tricky external links (with https).
      //
      14 => [
        'case' => [
          'uri' => 'https://local.tester',
          'filter' => '<a href="https://local.tester">Tricky external link</a>',
          'link' => [
            'link_text' => 'Tricky external link',
          ],
        ],
        'expectations' => [
          'external' => TRUE,
          'filter' => '<a href="https://local.tester" target="_blank" rel="noreferrer">Tricky external link</a>',
          'filter_customized' => '<a href="https://local.tester" target="_blank" rel="noopener nofollow noreferrer">Tricky external link</a>',
          'link' => [
            'attributes' => [
              'target' => '_blank',
              'rel' => 'noreferrer',
              'href' => 'https://local.tester',
            ],
            'link_text' => 'Tricky external link',
          ],
        ],
      ],
      15 => [
        'case' => [
          'uri' => 'https://local.tester/',
          'filter' => '<a href="https://local.tester/">Tricky external link with trailer slash</a>',
          'link' => [
            'link_text' => 'Tricky external link with trailer slash',
          ],
        ],
        'expectations' => [
          'external' => TRUE,
          'filter' => '<a href="https://local.tester/" target="_blank" rel="noreferrer">Tricky external link with trailer slash</a>',
          'filter_customized' => '<a href="https://local.tester/" target="_blank" rel="noopener nofollow noreferrer">Tricky external link with trailer slash</a>',
          'link' => [
            'attributes' => [
              'target' => '_blank',
              'rel' => 'noreferrer',
              'href' => 'https://local.tester/',
            ],
            'link_text' => 'Tricky external link with trailer slash',
          ],
        ],
      ],
      16 => [
        'case' => [
          'uri' => 'https://local.tester/user',
          'filter' => '<a href="https://local.tester/user">Tricky external link to the user page of an another Drupal site</a>',
          'link' => [
            'link_text' => 'Tricky external link to the user page of an another Drupal site',
          ],
        ],
        'expectations' => [
          'external' => TRUE,
          'filter' => '<a href="https://local.tester/user" target="_blank" rel="noreferrer">Tricky external link to the user page of an another Drupal site</a>',
          'filter_customized' => '<a href="https://local.tester/user" target="_blank" rel="noopener nofollow noreferrer">Tricky external link to the user page of an another Drupal site</a>',
          'link' => [
            'attributes' => [
              'target' => '_blank',
              'rel' => 'noreferrer',
              'href' => 'https://local.tester/user',
            ],
            'link_text' => 'Tricky external link to the user page of an another Drupal site',
          ],
        ],
      ],
      17 => [
        'case' => [
          'uri' => 'https://local.tester/user/login/',
          'filter' => '<a href="https://local.tester/user/login/">Link to the login page of an another Drupal site, with trailer slash</a>',
          'link' => [
            'link_text' => 'Link to the login page of an another Drupal site, with trailer slash',
          ],
        ],
        'expectations' => [
          'external' => TRUE,
          'filter' => '<a href="https://local.tester/user/login/" target="_blank" rel="noreferrer">Link to the login page of an another Drupal site, with trailer slash</a>',
          'filter_customized' => '<a href="https://local.tester/user/login/" target="_blank" rel="noopener nofollow noreferrer">Link to the login page of an another Drupal site, with trailer slash</a>',
          'link' => [
            'attributes' => [
              'target' => '_blank',
              'rel' => 'noreferrer',
              'href' => 'https://local.tester/user/login/',
            ],
            'link_text' => 'Link to the login page of an another Drupal site, with trailer slash',
          ],
        ],
      ],
      //
      // Other kind of tricky external links (with https).
      //
      18 => [
        'case' => [
          'uri' => 'https://drupal.org/local.test',
          'filter' => '<a href="https://drupal.org/local.test">External link</a>',
          'link' => [
            'link_text' => 'External link',
          ],
        ],
        'expectations' => [
          'external' => TRUE,
          'filter' => '<a href="https://drupal.org/local.test" target="_blank" rel="noreferrer">External link</a>',
          'filter_customized' => '<a href="https://drupal.org/local.test" target="_blank" rel="noopener nofollow noreferrer">External link</a>',
          'link' => [
            'attributes' => [
              'target' => '_blank',
              'rel' => 'noreferrer',
              'href' => 'https://drupal.org/local.test',
            ],
            'link_text' => 'External link',
          ],
        ],
      ],
      19 => [
        'case' => [
          'uri' => 'https://drupal.org/local.test/',
          'filter' => '<a href="https://drupal.org/local.test/">External link with trailing slash</a>',
          'link' => [
            'link_text' => 'External link with trailing slash',
          ],
        ],
        'expectations' => [
          'external' => TRUE,
          'filter' => '<a href="https://drupal.org/local.test/" target="_blank" rel="noreferrer">External link with trailing slash</a>',
          'filter_customized' => '<a href="https://drupal.org/local.test/" target="_blank" rel="noopener nofollow noreferrer">External link with trailing slash</a>',
          'link' => [
            'attributes' => [
              'target' => '_blank',
              'rel' => 'noreferrer',
              'href' => 'https://drupal.org/local.test/',
            ],
            'link_text' => 'External link with trailing slash',
          ],
        ],
      ],
      20 => [
        'case' => [
          'uri' => 'https://drupal.org/local.tester/user',
          'filter' => '<a href="https://drupal.org/local.tester/user">Another external link</a>',
          'link' => [
            'link_text' => 'Another external link',
          ],
        ],
        'expectations' => [
          'external' => TRUE,
          'filter' => '<a href="https://drupal.org/local.tester/user" target="_blank" rel="noreferrer">Another external link</a>',
          'filter_customized' => '<a href="https://drupal.org/local.tester/user" target="_blank" rel="noopener nofollow noreferrer">Another external link</a>',
          'link' => [
            'attributes' => [
              'target' => '_blank',
              'rel' => 'noreferrer',
              'href' => 'https://drupal.org/local.tester/user',
            ],
            'link_text' => 'Another external link',
          ],
        ],
      ],
      21 => [
        'case' => [
          'uri' => 'https://drupal.org/local.tester/user/',
          'filter' => '<a href="https://drupal.org/local.tester/user/">Another external link with trailing slash</a>',
          'link' => [
            'link_text' => 'Another external link with trailing slash',
          ],
        ],
        'expectations' => [
          'external' => TRUE,
          'filter' => '<a href="https://drupal.org/local.tester/user/" target="_blank" rel="noreferrer">Another external link with trailing slash</a>',
          'filter_customized' => '<a href="https://drupal.org/local.tester/user/" target="_blank" rel="noopener nofollow noreferrer">Another external link with trailing slash</a>',
          'link' => [
            'attributes' => [
              'target' => '_blank',
              'rel' => 'noreferrer',
              'href' => 'https://drupal.org/local.tester/user/',
            ],
            'link_text' => 'Another external link with trailing slash',
          ],
        ],
      ],
      //
      // Test cases with default target or rel attributes.
      //
      22 => [
        'case' => [
          'uri' => 'internal:/user/password',
          'filter' => '<a href="/user/password" target="_self" rel="nofollow">Internal link with attributes</a>',
          'link' => [
            'link_text' => 'Internal link with attributes',
            'options' => [
              'attributes' => [
                'target' => '_self',
                'rel' => 'nofollow',
              ],
            ],
          ],
        ],
        'expectations' => [
          'external' => FALSE,
          'filter' => '<a href="/user/password" target="_self" rel="nofollow">Internal link with attributes</a>',
          'link' => [
            'attributes' => [
              'target' => '_self',
              'rel' => 'nofollow',
              'href' => '/user/password',
            ],
            'link_text' => 'Internal link with attributes',
          ],
        ],
      ],
      23 => [
        'case' => [
          'uri' => 'https://example.com',
          'filter' => '<a href="https://example.com" target="_self" rel="nofollow">External link with attributes</a>',
          'link' => [
            'link_text' => 'External link with attributes',
            'options' => [
              'attributes' => [
                'target' => '_self',
                'rel' => 'nofollow',
              ],
            ],
          ],
        ],
        'expectations' => [
          'filter' => '<a href="https://example.com" target="_blank" rel="nofollow noreferrer">External link with attributes</a>',
          'filter_customized' => '<a href="https://example.com" target="_blank" rel="nofollow noopener noreferrer">External link with attributes</a>',
          'link' => [
            'attributes' => [
              'target' => '_blank',
              'href' => 'https://example.com',
              'rel' => 'nofollow noreferrer',
            ],
            'link_text' => 'External link with attributes',
          ],
        ],
      ],
      24 => [
        'case' => [
          'uri' => 'https://example.com',
          'filter' => '<a href="https://example.com" target="_blank" rel="noreferrer">External link with noreferrer attribute</a>',
          'link' => [
            'link_text' => 'External link with noreferrer attribute',
          ],
        ],
        'expectations' => [
          'filter' => '<a href="https://example.com" target="_blank" rel="noreferrer">External link with noreferrer attribute</a>',
          'filter_customized' => '<a href="https://example.com" target="_blank" rel="noreferrer noopener nofollow">External link with noreferrer attribute</a>',
          'link' => [
            'attributes' => [
              'target' => '_blank',
              'rel' => 'noreferrer',
              'href' => 'https://example.com',
            ],
            'link_text' => 'External link with noreferrer attribute',
          ],
        ],
      ],
      //
      // Email links.
      //
      25 => [
        'case' => [
          'uri' => 'mailto:a.user@example.com',
          'link' => [
            'link_text' => NULL,
            'menu_link_title' => 'Mail to a user',
          ],
        ],
        'expectations' => [
          'external' => TRUE,
          'link' => [
            'link_text' => 'a.user [at] example.com',
            'menu_link_title' => 'Mail to a user',
          ],
        ],
      ],
      26 => [
        'case' => [
          'uri' => 'mailto:a.user@example.com',
          'link' => [
            'link_text' => 'Mail to a user',
          ],
        ],
        'expectations' => [
          'external' => TRUE,
          'link' => [
            'link_text' => 'a.user [at] example.com (Mail to a user)',
            'menu_link_title' => 'Mail to a user',
          ],
        ],
      ],
    ];
  }

  /**
   * Returns url helper test cases.
   *
   * @return array
   *   The uri string to test for,and the expected result (TRUE or FALSE) for
   *   the url helper test.
   */
  public function selUrlHelperTestCases(): array {
    $test_cases = $this->selTestUris();
    array_walk($test_cases, function (&$item) {
      if (!isset($item['case']['uri']) || !isset($item['expectations']['external'])) {
        $item = NULL;
      }
      else {
        $item = [
          'url_input' => $item['case']['uri'],
          'external' => $item['expectations']['external'],
        ];
      }
    }, []);

    return array_values(array_filter($test_cases));
  }

  /**
   * Returns default link filter test cases.
   *
   * @return array
   *   The raw input and the expected output for the default filter_sel plugin
   *   test.
   */
  public function selLinkFilterDefaultTestCases(): array {
    $test_cases = $this->selTestUris();
    array_walk($test_cases, function (&$item) {
      if (!isset($item['case']['filter']) || !isset($item['expectations']['filter'])) {
        $item = NULL;
      }
      else {
        $item = [
          'filter_input' => $item['case']['filter'],
          'filter_expected' => $item['expectations']['filter'],
        ];
      }
    }, []);

    return array_values(array_filter($test_cases));
  }

  /**
   * Returns customized link filter's test cases.
   *
   * @return array
   *   The raw input and the expected output for the customized filter_sel
   *   plugin test.
   */
  public function selLinkFilterCustomizedTestCases(): array {
    $test_cases = $this->selTestUris();
    array_walk($test_cases, function (&$item) {
      if (!isset($item['case']['filter']) || !isset($item['expectations']['filter'])) {
        $item = NULL;
      }
      else {
        $expected_filtered = isset($item['expectations']['filter_customized']) ?
          $item['expectations']['filter_customized'] :
          $item['expectations']['filter'];
        $item = [
          'filter_input' => $item['case']['filter'],
          'filter_expected' => $expected_filtered,
        ];
      }
    }, []);

    return array_values(array_filter($test_cases));
  }

  /**
   * Returns the menu link test cases.
   *
   * @return array
   *   The menu link values, and the expected results for the menu link process
   *   test.
   */
  public function selMenuLinkTestCases(): array {
    $test_cases = $this->selTestUris();
    array_walk($test_cases, function (&$item) {
      // 'Filter' out invalid test cases.
      $item_source = $item;
      $item = NULL;
      if (
        isset($item_source['case']['uri']) &&
        (isset($item_source['case']['link']['link_text']) || $item_source['case']['link']['link_text'] === NULL) &&
        isset($item_source['expectations']['link']['link_text'])) {

        $link_title = isset($item_source['case']['link']['menu_link_title']) ?
          $item_source['case']['link']['menu_link_title'] :
          $item_source['case']['link']['link_text'];
        $link_options = isset($item_source['case']['link']['options']) ?
          $item_source['case']['link']['options'] :
          [];

        $item = [
          'menu_link_value' => [
            'title' => $link_title,
            'link' => [
              'uri' => $item_source['case']['uri'],
              'options' => $link_options,
            ],
          ],
        ];

        $expected_title = isset($item_source['expectations']['link']['menu_link_title']) ?
          $item_source['expectations']['link']['menu_link_title'] :
          $item_source['expectations']['link']['link_text'];

        $item['expectations'] = [
          'text' => $expected_title,
        ];

        if (isset($item_source['expectations']['link']['attributes'])) {
          $item['expectations']['attributes'] = $item_source['expectations']['link']['attributes'];
        }
      }
    }, []);

    return array_values(array_filter($test_cases));
  }

  /**
   * Returns the link formatter test cases.
   *
   * @return array
   *   The link values, and the expected results for the link formatter test.
   */
  public function selLinkFormatterTestCases(): array {
    $test_cases = $this->selTestUris();
    array_walk($test_cases, function (&$item) {
      // 'Filter' out invalid test cases.
      $item_source = $item;
      $item = NULL;
      if (
        isset($item_source['case']['uri']) &&
        (isset($item_source['case']['link']['link_text']) || $item_source['case']['link']['link_text'] === NULL) &&
        isset($item_source['expectations']['link']['link_text'])) {

        $link_options = isset($item_source['case']['link']['options']) ?
          $item_source['case']['link']['options'] :
          [];

        $item = [
          'link_value' => [
            'title' => $item_source['case']['link']['link_text'],
            'uri' => $item_source['case']['uri'],
            'options' => $link_options,
          ],
        ];

        $item['expectations'] = [
          'text' => $item_source['expectations']['link']['link_text'],
        ];

        if (isset($item_source['expectations']['link']['attributes'])) {
          $item['expectations']['attributes'] = $item_source['expectations']['link']['attributes'];
        }
      }
    }, []);

    return array_values(array_filter($test_cases));
  }

}
