<?php

namespace Drupal\Tests\sel\Traits;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpFoundation\Request;

/**
 * Host name helper.
 */
trait SelHostNameTrait {

  /**
   * The host.
   *
   * @var string
   */
  protected $hostName = 'local.test';

  /**
   * The scheme and the host.
   *
   * @var string
   */
  protected $schemeAndHost = 'http://local.test';

  /**
   * Mocks host name for Unit and Kernel tests.
   */
  protected function mockHostName(): void {
    $request = Request::createFromGlobals();
    $request->headers->set('HOST', $this->hostName);

    // Mocks a the request stack getting the current request.
    $request_stack = $this
      ->createMock('Symfony\Component\HttpFoundation\RequestStack');
    $request_stack->expects($this->any())
      ->method('getCurrentRequest')
      ->willReturn($request);

    // Unit tests don't have service container.
    try {
      $this->container->set('request_stack', $request_stack);
    }
    catch (\Throwable) {
      $container = new ContainerBuilder();
      $container->set('request_stack', $request_stack);

      // We need path.validator as well.
      $path_validator = $this->createMock('Drupal\Core\Path\PathValidatorInterface');
      $container->set('path.validator', $path_validator);
      \Drupal::setContainer($container);
    }
  }

}
