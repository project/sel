<?php

namespace Drupal\Tests\sel\Traits;

use Drupal\Core\Entity\Display\EntityFormDisplayInterface;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;

/**
 * BC layer for Drupal 8.7 and below.
 *
 * Will be removed after Drupal 8.7 gets unsupported.
 *
 * @internal
 */
trait SelTestEntityDisplayTrait {

  /**
   * Returns the form display entity of the given entity type bundle.
   *
   * @param string $entity_type
   *   The entity type.
   * @param string $bundle
   *   The bundle.
   * @param string $form_mode
   *   (optional) The form mode. Defaults to 'default'.
   *
   * @return \Drupal\Core\Entity\Display\EntityFormDisplayInterface
   *   The entity form display associated with the given form mode.
   */
  public function getEntityFormDisplay($entity_type, $bundle, $form_mode = 'default'): EntityFormDisplayInterface {
    $storage = $this->container->get('entity_type.manager')->getStorage('entity_form_display');

    // Try loading the entity from configuration; if not found, create a fresh
    // entity object. We do not preemptively create new entity form display
    // configuration entries for each existing entity type and bundle whenever a
    // new form mode becomes available. Instead, configuration entries are only
    // created when an entity form display is explicitly configured and saved.
    $entity_form_display = $storage->load($entity_type . '.' . $bundle . '.' . $form_mode);
    if (!$entity_form_display) {
      $entity_form_display = $storage->create([
        'targetEntityType' => $entity_type,
        'bundle' => $bundle,
        'mode' => $form_mode,
        'status' => TRUE,
      ]);
    }
    return $entity_form_display;
  }

  /**
   * Returns the view display entity of the given entity type bundle.
   *
   * @param string $entity_type
   *   The entity type.
   * @param string $bundle
   *   The bundle.
   * @param string $view_mode
   *   (optional) The view mode. Defaults to 'default'.
   *
   * @return \Drupal\Core\Entity\Display\EntityViewDisplayInterface
   *   The entity view display associated with the view mode.
   */
  public function getEntityViewDisplay($entity_type, $bundle, $view_mode = 'default'): EntityViewDisplayInterface {
    $storage = $this->container->get('entity_type.manager')->getStorage('entity_view_display');

    // Try loading the display from configuration; if not found, create a fresh
    // display object. We do not preemptively create new entity_view_display
    // configuration entries for each existing entity type and bundle whenever a
    // new view mode becomes available. Instead, configuration entries are only
    // created when a display object is explicitly configured and saved.
    $entity_view_display = $storage->load($entity_type . '.' . $bundle . '.' . $view_mode);
    if (!$entity_view_display) {
      $entity_view_display = $storage->create([
        'targetEntityType' => $entity_type,
        'bundle' => $bundle,
        'mode' => $view_mode,
        'status' => TRUE,
      ]);
    }
    return $entity_view_display;
  }

}
