<?php

namespace Drupal\Tests\sel\Kernel;

use Drupal\Component\Utility\Html;
use Drupal\KernelTests\KernelTestBase;
use Drupal\menu_link_content\Entity\MenuLinkContent;
use Drupal\system\Entity\Menu;
use Drupal\Tests\sel\Traits\SelHostNameTrait;
use Drupal\Tests\sel\Traits\SelLinkTestCasesTrait;

/**
 * Tests that menu links are well-handled.
 *
 * @group link
 */
class SelMenuLinkTest extends KernelTestBase {

  use SelHostNameTrait;
  use SelLinkTestCasesTrait {
    selMenuLinkTestCases as selMenuLinkTestDataProvider;
  }

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'link',
    'menu_link_content',
    'sel',
    'system',
    'user',
  ];

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The menu link plugin manager.
   *
   * @var \Drupal\Core\Menu\MenuLinkManagerInterface
   */
  protected $menuLinkManager;

  /**
   * The menu tree.
   *
   * @var \Drupal\Core\Menu\MenuLinkTreeInterface
   */
  protected $menuTree;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->mockHostName();

    $this->menuLinkManager = $this->container->get('plugin.manager.menu.link');
    $this->menuTree = $this->container->get('menu.link_tree');
    $this->renderer = $this->container->get('renderer');

    $this->installSchema('system', ['sequences']);
    $this->installSchema('user', ['users_data']);
    $this->installEntitySchema('menu_link_content');
    $this->installEntitySchema('user');
    $this->installConfig(['user', 'sel']);

    Menu::create([
      'id' => 'menu_test',
      'label' => 'Test menu',
      'description' => 'Description text',
    ])->save();
  }

  /**
   * Tests menu link processing.
   *
   * @dataProvider selMenuLinkTestDataProvider
   */
  public function testMenuProcessing($menu_link_value, $expectation): void {
    $menu_link_value['weight'] = 0;
    $menu_link_value['menu_name'] = 'menu_test';

    $menu_link = MenuLinkContent::create($menu_link_value);
    $menu_link->save();

    $build = $this->getTestMenuRenderable();
    $output = (string) $this->renderer->renderPlain($build);

    $this->assertEquals(FALSE, empty($output));

    $dom = Html::load($output);
    $xpath = new \DomXPath($dom);
    $menu_link = $xpath->query('//li/a')[0];

    $this->assertEquals($expectation['text'], $menu_link->textContent, "$menu_link->textContent ({$menu_link->getAttribute('href')}) fails with link text");

    if (!empty($expectation['attributes'])) {
      foreach ($expectation['attributes'] as $attr => $attr_val) {
        if ($attr_val === NULL) {
          $this->assertEquals(FALSE, $menu_link->hasAttribute($attr), "$menu_link->textContent fails with the $attr attribute: {$menu_link->getAttribute($attr)}");
        }
        else {
          $this->assertEquals($attr_val, $menu_link->getAttribute($attr), "$menu_link->textContent fails with the $attr attribute: expected $attr_val, actual: {$menu_link->getAttribute($attr)}");
        }
      }
    }
  }

  /**
   * Build the test menu 'menu_test'.
   */
  public function getTestMenuRenderable(): array {
    $parameters = $this->menuTree->getCurrentRouteMenuTreeParameters('menu_test');
    $parameters->setMinDepth(1);

    $tree = $this->menuTree->load('menu_test', $parameters);
    $manipulators = [
      ['callable' => 'menu.default_tree_manipulators:checkAccess'],
      ['callable' => 'menu.default_tree_manipulators:generateIndexAndSort'],
    ];
    $tree = $this->menuTree->transform($tree, $manipulators);
    return $this->menuTree->build($tree);
  }

}
