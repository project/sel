<?php

namespace Drupal\Tests\sel\Kernel;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;
use Drupal\link\LinkItemInterface;
use Drupal\node\Entity\Node;
use Drupal\node\Entity\NodeType;
use Drupal\Tests\sel\Traits\SelHostNameTrait;
use Drupal\Tests\sel\Traits\SelLinkTestCasesTrait;
use Drupal\Tests\sel\Traits\SelTestEntityDisplayTrait;
use Drupal\user\Entity\User;

/**
 * Tests that links link fields are well-handled.
 *
 * @group link
 */
class SelLinkFormatterTest extends EntityKernelTestBase {

  use SelHostNameTrait;
  use SelLinkTestCasesTrait {
    selLinkFormatterTestCases as selLinkFormatterTestDataProvider;
  }
  use SelTestEntityDisplayTrait;

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = [
    'node',
    'link',
    'spamspan',
    'sel',
  ];

  /**
   * The node view builder.
   *
   * @var \Drupal\Core\Entity\EntityViewBuilderInterface
   */
  protected $viewBuilder;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The link field name.
   *
   * @var string
   */
  protected $fieldName;

  /**
   * The test node.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected $node;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->mockHostName();

    $this->viewBuilder = $this->entityTypeManager->getViewBuilder('node');
    $this->renderer = $this->container->get('renderer');
    $this->fieldName = 'test_page_link_field';

    $type = NodeType::create([
      'type' => 'test_page',
      'name' => 'Test page',
    ]);
    $type->save();

    $this->installSchema('node', 'node_access');
    $this->installConfig(['system', 'node', 'sel']);

    // Create a field with settings to validate.
    $field_storage = FieldStorageConfig::create([
      'field_name' => $this->fieldName,
      'entity_type' => 'node',
      'type' => 'link',
      'cardinality' => FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED,
    ]);
    $field_storage->save();
    $field_config = FieldConfig::create([
      'field_storage' => $field_storage,
      'bundle' => 'test_page',
      'settings' => [
        'title' => DRUPAL_OPTIONAL,
        'link_type' => LinkItemInterface::LINK_GENERIC,
      ],
    ]);
    $field_config->save();
    $this->getEntityFormDisplay('node', 'test_page', 'default')
      ->setComponent($this->fieldName, [
        'type' => 'link_default',
        'settings' => [],
      ])
      ->save();
    $this->getEntityViewDisplay('node', 'test_page', 'default')
      ->setComponent($this->fieldName, [
        'type' => 'sel_link',
      ])
      ->save();

    $account = User::create(['name' => $this->randomString()]);
    $account->save();

    // Create a new node of the new node type.
    $this->node = Node::create([
      'type' => 'test_page',
      'title' => 'Test node',
      'uid' => $account->id(),
    ]);
  }

  /**
   * Tests link filter.
   *
   * @dataProvider selLinkFormatterTestDataProvider
   */
  public function testLinkFormatter($field_value, $expectation): void {
    $this->node->set($this->fieldName, $field_value);
    $this->node->save();

    $build = $this->viewBuilder->view($this->node, 'full');
    $output = (string) $this->renderer->renderPlain($build);

    $this->assertEquals(FALSE, empty($output));

    $dom = Html::load($output);
    $xpath = new \DomXPath($dom);
    $link_or_span = $xpath->query('//article/div/div/div/div/a | //article/div/div/div/div/span')[0];

    $this->assertEquals($expectation['text'], $link_or_span->textContent);

    if (!empty($expectation['attributes'])) {
      foreach ($expectation['attributes'] as $attr => $attr_val) {
        if ($attr_val === NULL) {
          $this->assertEquals(FALSE, $link_or_span->hasAttribute($attr));
        }
        else {
          $this->assertEquals($attr_val, $link_or_span->getAttribute($attr));
        }
      }
    }
  }

}
