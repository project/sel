<?php

namespace Drupal\Tests\sel\Unit;

use Drupal\Core\Url;
use Drupal\sel\Utility\SelUrlHelper;
use Drupal\Tests\sel\Traits\SelHostNameTrait;
use Drupal\Tests\sel\Traits\SelLinkTestCasesTrait;
use Drupal\Tests\UnitTestCase;

/**
 * Unit test for SelUrlHelper.
 *
 * @coversDefaultClass \Drupal\sel\Utility\SelUrlHelper
 * @group link
 */
class SelUrlHelperTest extends UnitTestCase {

  use SelHostNameTrait;
  use SelLinkTestCasesTrait {
    selUrlHelperTestCases as urlHelperTestDataProvider;
  }

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->mockHostName();
  }

  /**
   * Tests that the helper callback determines external urls properly.
   *
   * @covers ::isExternal
   *
   * @dataProvider urlHelperTestDataProvider
   */
  public function testIsExternal($url_input, $external): void {
    $url = Url::fromUri($url_input);
    $this->assertSame($external, SelUrlHelper::isExternal($url));
  }

  /**
   * Tests that the helper callback determines external urls properly.
   *
   * @covers ::isExternalUriString
   *
   * @dataProvider urlHelperTestDataProvider
   */
  public function testIsExternalUriString($url_input, $external): void {
    $this->assertSame($external, SelUrlHelper::isExternalUriString($url_input));
  }

}
