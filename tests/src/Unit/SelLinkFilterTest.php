<?php

namespace Drupal\Tests\sel\Unit;

use Drupal\Tests\sel\Traits\SelHostNameTrait;
use Drupal\sel\Plugin\Filter\SelLinkFilter;
use Drupal\Tests\sel\Traits\SelLinkTestCasesTrait;
use Drupal\Tests\UnitTestCase;

/**
 * Tests that links in formatted long text fields are well-handled.
 *
 * @coversDefaultClass \Drupal\sel\Plugin\Filter\SelLinkFilter
 * @group filter
 */
class SelLinkFilterTest extends UnitTestCase {

  use SelHostNameTrait;
  use SelLinkTestCasesTrait {
    selLinkFilterDefaultTestCases as linkFilterDefaultDataProvider;
    selLinkFilterCustomizedTestCases as linkFilterCustomizedDataProvider;
  }

  /**
   * SelLinkFilter with default settings.
   *
   * @var \Drupal\sel\Plugin\Filter\SelLinkFilter
   */
  protected $filterDefault;

  /**
   * SelLinkFilter with customized settings.
   *
   * @var \Drupal\sel\Plugin\Filter\SelLinkFilter
   */
  protected $filterCustomized;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->mockHostName();

    $config_default['settings'] = [
      'rel_required' => 'noreferrer',
      'rel_optionals' => [],
    ];

    $config_customized['settings'] = [
      'rel_required' => 'noopener',
      'rel_optionals' => ['nofollow', 'noreferrer'],
    ];

    $this->filterDefault = new SelLinkFilter($config_default, 'filter_sel', ['provider' => 'sel']);
    $this->filterCustomized = new SelLinkFilter($config_customized, 'filter_sel', ['provider' => 'sel']);
  }

  /**
   * Tests link filter with noreferrer and without optionals.
   *
   * @param string $html
   *   Input HTML.
   * @param string $expected
   *   The expected output string.
   *
   * @dataProvider linkFilterDefaultDataProvider
   */
  public function testSelLinkFilter($html, $expected): void {
    $this->assertSame($expected, (string) $this->filterDefault->process($html, 'en'));
  }

  /**
   * Tests link filter with noopener (required), nofollow and noreferrer.
   *
   * @param string $html
   *   Input HTML.
   * @param string $expected
   *   The expected output string.
   *
   * @dataProvider linkFilterCustomizedDataProvider
   */
  public function testCustomizedSelLinkFilter($html, $expected): void {
    $this->assertSame($expected, (string) $this->filterCustomized->process($html, 'en'));
  }

}
